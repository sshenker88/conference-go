from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .acl import get_photo_url, get_weather


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "location_url"

    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder,
    }

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name"
    ]

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else: ## POST
        #get_photo_url()
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            {
            "conference": conference,
            "weather": get_weather(conference.location.city, conference.location.state.name)},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        conference = Conference.objects.filter(id=id)
        count, _ = conference.delete()
        return JsonResponse({"deleted": count > 0})
    else: ### PUT
        data = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=data["state"])
            data["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
        )
        Conference.objects.filter(id=id).update(**data)
        conference = Location.objects.get(id=id)
        return JsonResponse(conference, encoder=ConferenceDetailEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations":locations},
            encoder=LocationListEncoder,
            safe=False)
    else: ## POST
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400,
            )
        content["location_url"] = get_photo_url(content["city"], content["state"].name)
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    location = Location.objects.filter(id=id)
    if request.method == "GET":
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = location.delete()
        return JsonResponse({"deleted": count > 0})
    else: ## PUT
        data = json.loads(request.body)
        print(data)
        try:
            state = State.objects.get(abbreviation=data["state"])
            data["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
        )
        Location.objects.filter(id=id).update(**data)
        location = Location.objects.get(id=id)
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)
