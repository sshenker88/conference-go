import requests
from events.keys.keys import PEXELS_key, OPEN_WEATHER_API_KEY


def get_photo_url(city, state):

    header = {"Authorization": PEXELS_key}

    data = requests.get(f'https://api.pexels.com/v1/search?query={city}+{state}&per_page=1', headers=header)
    dict = data.json()
    print(dict["photos"][0]['url'])
    return dict["photos"][0]['url']

# get_photo_url("Boston", "Massachusetts")


def get_weather(city, state):
    params = {"q": f'{city},{state},US', "appid": OPEN_WEATHER_API_KEY}
    url = "http://api.openweathermap.org/geo/1.0/direct"

    response = requests.get(url, params=params)
    content = response.json()
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except:
        return None

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    w_content = response.json()
    try:
        temp = w_content["main"]["temp"]
        desc = w_content["weather"][0]["description"]
    except:
        return None

    return {"temp": temp, "description": desc}

