from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from events.models import Conference
from django.views.decorators.http import require_http_methods
import json

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        ## status
        ## conference {name, href}
    ]

    def get_extra_data(self, o):
        return {
            "status": o.status.name,
            "conference":{
                "name": o.conference.name,
                "href": o.conference.get_api_url(),
            }
        }

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        #"status",
        # "href",
    ]
    def get_extra_data(self, o):
        return {
            "status": o.status.name,
            "href": o.conference.get_api_url(),
        }

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        p = Presentation.objects.filter(conference=conference_id)
        return JsonResponse({"presentations": p}, encoder=PresentationListEncoder)
    else: # POST
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=content["conference"])
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        data = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=data["conference"])
        except Conference.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid conference"},
            status=400,
        )
        Presentation.objects.filter(id=id).update(**data)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(presentation, encoder=PresentationDetailEncoder, safe=False)
    else: ## DELETE
        presentation = Presentation.objects.filter(id=id)
        count, _ = presentation.delete()
        return JsonResponse({"deleted": count > 0})
